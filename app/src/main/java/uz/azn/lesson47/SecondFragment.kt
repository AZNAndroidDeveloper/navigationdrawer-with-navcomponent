package uz.azn.lesson47

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.fragment.app.Fragment
import uz.azn.lesson47.databinding.FragmentSecondBinding

class SecondFragment : Fragment(R.layout.fragment_second) {
    private lateinit var binding: FragmentSecondBinding

    private var downX = 0.0f
    private var downY = 0.0f

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSecondBinding.bind(view)
        with(binding) {
            imageView.setOnTouchListener { v, event ->
                // qol tegan holat
                when (event.actionMasked) {
                    MotionEvent.ACTION_DOWN -> {
                        downX = event.x
                        downY = event.y

                    }
                    // harakat toxtagan  holat
                    MotionEvent.ACTION_MOVE->{
                        var movedX = 0.0f
                        var movedY = 0.0f
                        movedX = event.x
                        movedY = event.y
                        // harakatni qancha bolganini aniqlash

                        val diffX = movedX-downX
                        val diffY = movedY-downY
imageView.x = imageView.x+diffX
                        imageView.y = imageView.y + diffY
                    }
                }

                return@setOnTouchListener true
            }
        }
    }
}