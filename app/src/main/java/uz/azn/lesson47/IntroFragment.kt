package uz.azn.lesson47

import android.os.Bundle
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.view.View
import android.view.animation.OvershootInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import uz.azn.lesson47.databinding.FragmentIntroBinding

class IntroFragment ():Fragment(R.layout.fragment_intro){
    private lateinit var binding:FragmentIntroBinding
    private var isInfoLayout = false
private lateinit var constraintLayout:ConstraintLayout
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    binding = FragmentIntroBinding.bind(view)
        with(binding){
            constraintLayout.setOnClickListener {
                if (isInfoLayout){
                    changeLayout(R.layout.fragment_intro)
                }
                else{
                    changeLayout(R.layout.fragment_intro_info)
                }

            }
        }

    }

private fun changeLayout(layoutId:Int){
val constraintSet = ConstraintSet()
    constraintSet.clone(requireContext(),layoutId)

    val transition = ChangeBounds()
    transition.interpolator = OvershootInterpolator(1.0f)
     transition.duration = 1000
    TransitionManager.beginDelayedTransition(binding.constraintLayout, transition)
    constraintSet.applyTo(binding.constraintLayout)
    isInfoLayout = !isInfoLayout

}
}